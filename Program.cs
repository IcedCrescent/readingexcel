﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ReadingExcel
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"";
            try
            {
                FileInfo exisitingFile = new FileInfo(filePath);
                if (Directory.Exists(filePath))
                {
                    Console.WriteLine("Please specify the path to the file, not a directory in filePath vairable");
                    goto exit;
                }

                // Open the workbook by passing the FileInfo, creak a new workbook by leaving the constructor blank 
                using (ExcelPackage package = new ExcelPackage(exisitingFile))
                {
                    // get the first worksheet in the workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    // output column value
                    Console.WriteLine($"\t{worksheet.Cells[1, 2].Value}");
                    for (int row = 2; row <= 12; row++)
                    {
                        Console.WriteLine("Cells({0},{1}).Value= {2}", row, 2, worksheet.Cells[row, 2].Value);
                    }
                } // the using statement automatically calls Dispose() which closes the package.
            }
            catch (Exception)
            {
                Console.WriteLine("Please specific the workbook path in filePath variable, and make sure it's the valid path");
            }
            
            exit:
            Console.ReadKey();
        }
    }
}
